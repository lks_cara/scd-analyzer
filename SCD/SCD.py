import os,pdb,datetime,subprocess
from os.path import splitext,basename

# these are the settings the script uses
sett = {"outfolder": "SCD_results","file_format": "fasta","reviewed":"yes","match": "","n": 3,"range":100}
log_file = "SQTQ.log"
log_level = 4

def perform_ps_scan(fasta_file):
	'''Calls the Perl Script that searches through the fasta file for proteins with SCD domains.
	The SCD domains that our counted have extended distances.  For example, a search for 3 SCDs within
	50 AA would actualy return SCDs domains within 100 AA.  To filter to only include 50 AA, use the
	perform_length_restriction() function
	Returns the results as a list.
	'''
	
	# create query, n is the number of SQT to find in the 'range'
	query = (sett["n"]-1)*"[ST]Q-X(0,{})-".format(sett["range"])+"[ST]Q"	
	program_name = "perl"
	arguments = ["ps_scan/ps_scan.pl", "-g","-ogff","-p"+query,fasta_file]
	if not os.path.exists(sett['outfolder']):
		os.makedirs(sett['outfolder'])
	outpath = os.path.join(sett['outfolder'],str(sett["n"])+"SCD"+str(sett["range"])+"_"+splitext(basename(fasta_file))[0] + "_ps_scan.gff")
	if os.path.exists(outpath): return outpath
	log("Running ps_scan Perl script with arguments: {}".format(arguments),2)
	command = [program_name]
	command.extend(arguments) #combines the 'perl' command with its arguments into one list
	out_writer = open(outpath,"wb")
	out = subprocess.Popen(command,stdout=out_writer)
	retcode = out.wait()
	out_writer.close()
	data = open(outpath).read()
	log("Number of proteins found with ps_scan\t"+ str(len(data.split('\n'))))
	
	if not os.path.exists(sett['outfolder']): os.mkdir(sett['outfolder'])
	return outpath


def perform_length_restriction(gff_file):
	''' Filters out all SCD results that have a greater length than the desired range.
	'''
	outpath = os.path.join(sett['outfolder'],splitext(basename(gff_file))[0] + "_length_filt.gff")
	if os.path.exists(outpath): return outpath
	data,count = open(gff_file).read(),0
	with open("list.txt","wb")as d:
		with open(outpath,'wb') as out:
			for line in data.split("\n"):				
				if not line == "":
					start,end = line.split()[3],line.split()[4]					
					if (int(end) - int(start)) < sett['range']:
						out.write(line + "\n")
						count+=1
						d.write(str(int(end) - int(start))+"\n")
						
	log("Number of proteins after length filtering\t"+str(count))
	return outpath

def perform_multiple_removal(gff_file):
	# remove duplicate protein hits
	outpath = os.path.join(sett['outfolder'],splitext(basename(gff_file))[0] + "_mult_filt.gff")
	if os.path.exists(outpath): return outpath
	added,name,count = [],"",0
	
	data = open(gff_file).read()
	with open(outpath,'wb') as out:
		for line in data.split('\n'):
			if not line == "": 
				name = line.split()[0].split("|" )[3]
				# if the protein has not already returned, add it
				if not name in added: 
					out.write(line + "\n")
					added.append(name)
					count += 1
	log("Number of proteins after duplicates removed\t"+str(count))
	return outpath
def perform_isoform_restriction(gff_file):
	outpath = os.path.join(sett['outfolder'],splitext(basename(gff_file))[0] + "_isoform_filt.gff")
	if os.path.exists(outpath): return outpath
	data,count,count_iso,count_iso_rem = open(gff_file).read(),0,0,0
	added = [] # keeps track of which protein isforms have been added by comparing the discription string
	with open(outpath,'wb') as out:
		for line in data.split("\n"):
			if not line == "":
				name = line.split("|")[-1].split("isoform")
				# 'isoform' was found in description. add the description to the added list and print out the protein result
				if len(name) > 1: 
					count_iso +=1
					if name[0] not in added:
						added.append(name[0])
						out.write(line + "\n")
						count+=1
					else:
						count_iso_rem +=1
				else:
					out.write(line + "\n")
					count+=1
	log("Number of proteins with isoforms\t"+str(count_iso))
	log("Number of proteins with isoforms removed\t"+str(count_iso_rem))
	log("Number of proteins after isoform filtering\t"+str(count))
	return outpath

def log(msg,level=2):
	lev = {0: "ERROR",1:"WARNING",2:"INFO",3:"DEBUG"}
	now = datetime.datetime.now()
	line = "{}:\t{}\t{}\n".format(now,lev[level],msg)

	# output the log to the console and the file
	if level <= log_level:
		print line
		with open(log_file,"a") as f:
			f.write(line)

def run_scd(outfolder= "results", file_format= "fasta",reviewed= "yes",match= "",n= 3, range= 100):
	'''
	This function exists so that the code can be run from other python scripts.
	Returns the path containing all SCD proteins.
	'''
	global sett
	sett = {"outfolder": outfolder ,"file_format": file_format ,"reviewed": reviewed,"match": match ,"n": n ,"range": range}
	files_fasta = [f for f in os.listdir(os.getcwd()) if os.path.isfile(f) and ".fasta" in f]
	results = []
	for f in files_fasta:
		res_path = perform_ps_scan(f)
		res_path = perform_length_restriction(res_path)
		res_path = perform_multiple_removal(res_path)
		res_path = perform_isoform_restriction(res_path)
		results.append(res_path)
	return results

if __name__ == "__main__":
	files_fasta = [f for f in os.listdir(os.getcwd()) if os.path.isfile(f) and ".fasta" in f]
	for f in files_fasta:
		res_path = perform_ps_scan(f)
		res_path = perform_length_restriction(res_path)
		res_path = perform_multiple_removal(res_path)
		res_path = perform_isoform_restriction(res_path)




