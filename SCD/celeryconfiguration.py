BROKER_TRANSPORT = "redis"

redis_port = 7775 # The port number that redis should use
BROKER_HOST = "localhost"  # Maps to redis host.
BROKER_PORT = redis_port         # Maps to redis port.
BROKER_VHOST = "0"         # Maps to database number.

CELERY_RESULT_BACKEND = 'redis://localhost:{}/0'.format(redis_port)

CELERY_IMPORTS = ("SCD",)

CELERYD_CONCURRENCY = 1

CELERYD_AUTOSCALER = 1,1