import os
import sqlite3 as lite
import utilities as ut
from Bio import SeqIO
import pdb

#Directory of Database
db_string = "/home/lukas/Documents/scd_db/SCD.db"
def base_name(k):
	return os.path.basename(k).split(".")[0]

#data_dir="C:\Users\Franklin\Documents\scd_db"
# for ipython C:\Users\Franklin\Dropbox\scd-analyzer\SCD

def idParse(h): #used to remove junk chars from ID after retrieving from DB
	rows = h.fetchone()
	rows = list(rows)
	listRow = rows[0].split("/'")
	return listRow[0]

def createDatabase(db_string):  
	data_dir = os.path.split(db_string)[0]
	fasta_dir = os.path.join(data_dir,"fasta")
	fasta_files = [ os.path.join(fasta_dir,f) for f in os.listdir(fasta_dir) if os.path.isfile(os.path.join(fasta_dir,f)) 
													and f.endswith(".fasta")]
	results_dir = os.path.join(data_dir,"scd_results")
	results_files = [ os.path.join(results_dir,f) for f in os.listdir(results_dir) if os.path.isfile(os.path.join(results_dir,f)) 
													and f.endswith("length_filt.gff")]
	tempArray=[]
	#Database location
	db_string = os.path.join(data_dir,"SCD.db")
	con = lite.connect(db_string)
	cur = con.cursor()
	table_name = 'fasta' #+ base_name(f)
	drop_SQL= "DROP TABLE IF EXISTS " + table_name 
	cur.execute(drop_SQL)
	table_SQL= "CREATE TABLE IF NOT EXISTS " + table_name +"(RefSeq TEXT PRIMARY KEY NOT NULL, Description TEXT, Sequence TEXT);"
	cur.execute(table_SQL)
	for f in fasta_files:
		print "Processing: ", f
		handle = open(f,'rU')
		data = SeqIO.parse(handle, "fasta") 
		for i,d in enumerate(data):
			tempArray = d.description.split("|")
			d.description = tempArray[4]
			d.id = tempArray[3][:-2]
			insert="INSERT into " + table_name + "(RefSeq, Description, Sequence)" + " VALUES(\""+ d.id + "\", \"" + d.description + "\", \"" + d.seq.tostring() +"\")" 
			cur.execute(insert)
	con.commit()
	cur.close()
	print "Database Creation Complete"

	temp = []
	results = []
	fileNameAttri = []
	con = lite.connect(db_string)
	cur = con.cursor()
	cur.execute("DROP TABLE IF EXISTS Results")
	cur.execute("CREATE TABLE IF NOT EXISTS Results(id INTEGER PRIMARY KEY AUTOINCREMENT, RefSeq TEXT NOT NULL, Definition TEXT NOT NULL, start INT NOT NULL, end INT NOT NULL, scd_sequences TEXT NOT NULL);")
	
	for f in results_files:		
		table_name = str(base_name(f)).replace("_ps_scan_length_filt","")
		fileNameAttri =  base_name(f).split("_")
		#print fileNameAttri[0] + ", " + fileNameAttri[1]
		# 		def            			 fastafile
		print table_name
		results = ut.parse_results(f)
		for j, e in enumerate(results):
			temp = results[j]["id"].split("|")
			results[j]["id"] = temp[3][:-2]
			#results[j]["id"] is the refseq
			#print results[j]["id"]
			cur.execute("INSERT INTO Results (RefSeq, Definition, start, end, scd_sequences) VALUES (?,?,?,?,?)", ((results[j])["id"], fileNameAttri[0], results[j]["scd_start"], results[j]["scd_end"], results[j]["scd_sequence"]))
	con.commit()
	cur.close()

def databaseResultsOut(refseqID):
	con = lite.connect(db_string)
	cur = con.cursor()
	sql = "SELECT fasta.RefSeq, Description, Sequence, Definition, start, end, scd_sequences from fasta INNER JOIN Results on fasta.Refseq = Results.RefSeq WHERE fasta.RefSeq = \'" + refseqID + "\';"
	#SELECT fasta9606.RefSeq, Description, Sequence, Definition, start, end, scd_sequences FROM fasta9606 INNER JOIN Results on fasta9606.RefSeq = Results.RefSeq WHERE fasta9606.RefSeq = 'NP_001010879';
	cur.execute(sql)
	rows = cur.fetchall()
	#print len(rows)
	for row in rows:
		print row
	return list(rows) #option to output the results as a list

def databaseFASTAOut(fastaFile):
	con = lite.connect('database.db')
	cur = con.cursor()
	search_SQL = "SELECT * from table" + str(fastaFile)
	cur.execute(search_SQL)
	rows = cur.fetchall()
	for row in rows:
		print row

if __name__ == "__main__":
	global fasta_dir
	media = os.path.abspath(os.path.join(".","frontend/media"))
	parser = argparse.ArgumentParser(prog="python -m grsnp.server", description="Starts the SCD Analyzer server. Example: python -m grsnp.server -d /home/username/scd-analyzer/ -p 8000", epilog="Use SCD Analyzer: http://localhost:8000/scd")
	parser.add_argument("--data_dir" , "-d", nargs="?", help="Set the directory containing the database. Required. Use absolute path. Example: /home/username/scd_db/.", default="")
	parser.add_argument("--port","-p", nargs="?", help="Socket port to start server on. Default: 8081", default=8081) 
#drop table if alrdy exixts