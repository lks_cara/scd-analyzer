function get_progress(){
	$.post(root_url + "/get_progress?id="+run_id, function(data){
		if (data == "") { return; }
		prog = jQuery.parseJSON(data);
		$("#progressbar").progressbar({
							value: prog["curprog"],
							max:  prog["progmax"]
						});
		$('#status').text(prog["status"]);
		if (prog["status"].toLowerCase() == "analysis completed!"){
			location.reload()
			clearInterval(progID);
		}
	});
}

function load_results(){
	$.post(root_url+"/get_summary?id="+run_id, function(data){
		if (data==""){ return; }
		$("#summary_results").html(data);
	});
}

function getProteinList(res_filename,title){
	// Load result list and create links to the actual protein sequence.
	$.post(root_url+"/get_allprots_results?id="+run_id+"&res_filename="+res_filename, function(data){
		if (data==""){ return; }	
		data = data.split("\n");
		var scd_desc = res_filename.split("_")[0];
		scd_desc = scd_desc.split("SCD")[0] + " SCD in " + scd_desc.split("SCD")[1];
		$("#well_fasta_results").hide();
		$("#well_fasta_results").html("<h3><center><u>Analysis Results for '"+scd_desc+", "+title+"'</u></center></h3>")
		$.each(data, function(i,d) {
			if (d!="")  {
				// extract SCD results sequence from results
				scd_sequence = d.split("\t")[8].split(";")[0].replace("Sequence \"","").replace("\"",""); 
				// remove all '.' characters
				scd_sequence = scd_sequence.split(".").join("");
				// Create link for result and list it.
				args = "'" + d.split("|")[3].split(".")[0].split("\"").join("")+"','"+ d.split("\t")[3] +"','" +
										 d.split("\t")[4]+ "','"+scd_sequence.trim()+"'";

				$("#well_fasta_results").append("<a  href=\"#\" onclick=\"get_sequence(" +args+")\">"
										+d.split("SequenceDescription")[1].split("\"").join("")+"</a><br/>");
			}

		});
		$("#well_fasta_results").show();
	});
}

function get_sequence(prot_id,scd_start,scd_end,scd_sequence){
	$.post(root_url+"/get_sequence?ref_id="+prot_id, function(data){
		data = jQuery.parseJSON(data);
		data[2] = data[2].trim();
		// data[0] is refSeq, data[1] is description, data[1] is fasta sequence
		$("#modal_sequence").html("<div style='width:50em;height:Auto' class='well break-word'><p><b>" + data[0] + ":" + data[1] + "</b></br>" +  data[2].slice(0,scd_start-1) + "<span class='scd'>" + color_SCD(scd_sequence) + "</span>" + data[2].slice(scd_end,data[2].length) +"</p></div>")
		$("#modal_result").modal({show:true});
	});
}

function color_SCD(scd_seq){
	html_seq = "";
	for (var k = 0; k < scd_seq.length; k++){
		var c = scd_seq[k];
		if (c.toLowerCase() !=  c) { // Check if uppercase, if so found STQ. Mark it.
			html_seq += "<span class='STQ'>" + scd_seq[k] + scd_seq[k+1] + "</span>"
			k++; // increment by since we want to skip the next character (STQ is always made of two characters).
		}
		else{
			html_seq += c;
		}
	}
	return html_seq.toUpperCase();
}


// Show loading message when loading results
$(document).ajaxStart(function(){
  myModal.showPleaseWait();	
});
$(document).ajaxComplete(function(){
  myModal.hidePleaseWait();
});

var myModal;
// The following code was adapted from:
// http://dotnetspeak.com/2013/05/creating-simple-please-wait-dialog-with-twitter-bootstrap/comment-page-1
myModal = myModal || (function () {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };
})();