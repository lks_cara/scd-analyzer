var fasta_result_data = {};
var cur_fasta_index = {};
var num_to_load = {};


function load_fasta_results(fasta_name,strid){
	cur_fasta_view = 0;
	if ($("#"+strid).children().length > 1) {return;} 	

	// function is used by ajax as callback
	var ajax_fasta = function(strid){
		return function(data,textStatus){
			if (data =="[]") { 
				var container = $("#"+strid); // must use this method of selection by id in order to account for file names with spaces in them.
				container.html("<h4 id='remove'><center>None of the uploaded proteins contained SCDs.</center></h4>")
				return;
			}
			if (data==""){
				console.log("AJAX: no data returned");
				return;
			}
			fasta_result_data[strid] = jQuery.parseJSON(data);						
			cur_fasta_index[strid] = 0; // keeps track of which results should be loaded
			num_to_load[strid] = fasta_result_data[strid].length; // set to load all data
			// reattempt to load the results
			load_fasta_results(fasta_name,strid);
			load_more_data(strid)
		}
	}

	// check if data has already been retrieved from the server
	if (typeof fasta_result_data[strid] == "undefined"){
		// Load the data into a global array
		res_name = fasta_name.split("_ps_scan")[0].split("_").slice(1).join("_") + "_"+strid
		$.post(root_url+'/get_json_results?id='+run_id+'&result_name='+res_name, ajax_fasta(strid));
	}
	// if the data has been loaded, draw the results
	else {		
		load_more_data(strid);	
	}

		
}

function get_log(){
	$.post(root_url+"/get_log?id="+run_id, function(data){
		if (data == "") { return; }		
		$("#txtLog").text(data)
	});
}

// This function displays the colored fasta results
function load_more_data(strid){

	 // check if results have been rendered
	if ($("#w_"+strid).children().length > 0) {
		// show existing rendered results.
		show_results(strid);
		return;
	} 
	tmp = cur_fasta_index[strid] + num_to_load;
	cur_results = fasta_result_data[strid];
	var container = $("#w_"+strid); // must use this method of selection by id in order to account for file names with spaces in them.
	container.html("<h4 id='remove'><center>Processing results. Please wait.</center></h4>")
	// load the next results and append them to the bottom of the container
	for (var i=cur_fasta_index[strid]; i<cur_fasta_index[strid]+num_to_load[strid];i++){
		if (i >= cur_results.length){
			cur_fasta_index[strid] =  i;
			return;
		}

		scd_start = cur_results[i]["scd_start"];
		scd_end = cur_results[i]["scd_end"];
		container.append("<div style='width:50em;' class='well break-word'><p><b>" + cur_results[i]["id"] + cur_results[i]["description"] + "</b></br>" + 
		cur_results[i]["sequence"].slice(0,scd_start-1) + "<span class='scd'>" + color_SCD(cur_results[i]["scd_sequence"]) + "</span>" + cur_results[i]["sequence"].slice(scd_end,cur_results[i]["sequence"].length) +"</p></div>");

	}
	$("#remove").remove()
}

function getProteinList(file,description){
	// This function was created to prevent having to rewrite
	// this Scripts after support for tabs was dropped in favor 
	// of creating links in the summary box.
	// This relies on the description field set in the function call 
	// described in the summary.html file.
	strid = ""
	switch (description){
		case "All Proteins":
			strid = "all"
			break
		case "Multiple isoforms":
			strid = "single"
			break
		case "Single SCD per protein":
			strid = "isoforms"
			break
	}
	load_fasta_results(file,strid)
	load_more_data(strid);
}

function show_results(strid){
	// Collapse all the results
	// They can be redisplayed if they exist.
	$("#w_all").css("display","None");
	$("#w_single").css("display","None");
	$("#w_isoforms").css("display","None");
	$("#w_"+strid).css("display","Inherit");
	// hide selection text
	$('#msg_select_results').html('')
}

function load_table(fasta_name){
	var cols = [{"short": "all", "long": "All SCD proteins"},{"short": "single","long": "Results restricted to single SCD per protein"},
				{"short": "isoforms", "long": "Results restricted to single SCD isoform (Only if supported)"}];
	// load in the data
	$(cols).each(function (i,d){
		// check if the data has already been retrieved for this column. If not retrieve via Ajax
		if (typeof fasta_result_data[d["short"]] == "undefined"){
			// Mark this column data as being retrieved
			fasta_result_data[d["short"]] = "Retrieving";
			// if data is not loaded do an ajax call
			$.post(root_url+'/get_json_results?id='+run_id+'&result_name='+ fasta_name + "_" + d["short"],function(txt){
				fasta_result_data[d["short"]] = jQuery.parseJSON(txt);				
				cur_fasta_index[d["short"]] = 0; // keeps track of which results should be loaded
				num_to_load[d["short"]] = fasta_result_data[d["short"]].length // set to load all data
				// call back must once again try to load the data
				load_table();
			});			
		}
		// ensures all data has been loaded before rendering the text
		else if (Object.keys(fasta_result_data).length == 3) {
			// convert data to format used by the table.
			data = [];
			$(cols).each(function(i_c,d_c){ 
				data.push($(fasta_result_data[d_c.short]).map(function(k,t){
					return t.id;
				}).get());
			});
			$(cols).each(function (i_c,d_c){
				max_size = 0;
				str_d = "";
				// Create header line
				$(cols).each(function(id,d_c){
					str_d += d_c.long + "\t";
				});
				str_d += "\n";

				// get lengths of data columns
				cols_lengths = [];
				$(data).each(function(id,d_c){ 
					if (d_c.length > max_size) { 
						max_size = d_c.length;
					};
				});
				// add the rest of the data to the string.
				for (var i_row = 0; i_row < max_size; i_row++){
					for (var i_col = 0; i_col < 3 ; i_col++) {
						if (typeof data[i_col][i_row] == "undefined") { 
							str_d += "\t";
						}
						else {
							str_d += data[i_col][i_row] + "\t";
						};
					};
					str_d += "\n";
				};
				$("#txt_results").text(str_d);
			});
		}
	});
}

function get_progress(){
	$.post(root_url + "/get_progress?id="+run_id, function(data){
		if (data == "") { return; }
		prog = jQuery.parseJSON(data);
		$("#progressbar").progressbar({
							value: prog["curprog"],
							max:  prog["progmax"]
						});
		$('#status').text(prog["status"]);
		if (prog["status"].toLowerCase() == "analysis completed!"){
			location.reload()
			clearInterval(progID);
		}
	});
}


function color_SCD(scd_seq){
	html_seq = "";
	for (var k = 0; k < scd_seq.length; k++){
		var c = scd_seq[k];
		if (c.toLowerCase() !=  c) { // Check if uppercase, if so found STQ. Mark it.
			html_seq += "<span class='STQ'>" + scd_seq[k] + scd_seq[k+1] + "</span>"
			k++; // increment by since we want to skip the next character (STQ is always made of two characters).
		}
		else{
			html_seq += c;
		}
	}
	return html_seq.toUpperCase();
}

// Show loading message when loading results
$(document).ajaxStart(function(){
  myModal.showPleaseWait();	
});
$(document).ajaxComplete(function(){
  myModal.hidePleaseWait();
});

var myModal;
// The following code was adapted from:
// http://dotnetspeak.com/2013/05/creating-simple-please-wait-dialog-with-twitter-bootstrap/comment-page-1
myModal = myModal || (function () {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    return {
        showPleaseWait: function() {
            pleaseWaitDiv.modal();
        },
        hidePleaseWait: function () {
            pleaseWaitDiv.modal('hide');
        },

    };
})();