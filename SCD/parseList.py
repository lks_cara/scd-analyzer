
#Taking in the input and corresponding results
#input1 = open(r"C:\Users\Franklin\Documents\scd_db\run_files_dir\results\t5qyhr331kwfqj2thquawkhz6ah2ib83\input.fasta")
#results = open(r"C:\Users\Franklin\Documents\scd_db\run_files_dir\results\t5qyhr331kwfqj2thquawkhz6ah2ib83\results.txt")

def merge_results_fasta(input1,results):
    #Parsing the Input into x[general information][amino acid sequence]
    x=[]
    y=[]
    i=0
    for line in input1: 
        x.append(line)
    x = ''.join(x)
    x=x.replace("\n","")
    x=x.split(">")
    x=x[1:]
    for entry in x:
        x[x.index(entry)]= entry.split("]")
    for entry1 in x:
        x[x.index(entry1)][1] = x[x.index(entry1)][1].lower()

    #Substituting the Results into the Input
    clist = []
        #Indexes: 0=GI 1=RefSeq 2=psScanVersion 3=user 4=FirstSTQpos 5=LastSTQpos 6=Sequence 7=Description    
    for line in results:
        #splits line based on tabs
        splitStr = line.split("\t")
        
        #splits the last item in splitStr into two items, sequence and sequence description
        lastItem = splitStr[len(splitStr)-1]
        splitLastItem = lastItem.split(";")

        #adds the split items into the list of splitStr
        splitStr[len(splitStr)-1]= splitLastItem[0]
        splitStr.append(splitLastItem[1])

        #splits the first item into four and appends all other items in splitStr to a new list called entry
        entry = splitStr[0].split("|")
        for i in range(1,len(splitStr)):
            entry.append(splitStr[i])

        #Results Editing 1.0: modifying line to remove blank indexes
        del entry[11]
        del entry[10]
        del entry[9]
        del entry[4]
        del entry[2]
        del entry[0]

        #Results Editing 1.1: removing excess text from results
        temp = entry[6].split(" ")
        del entry[6]
        entry.insert(6,temp[1])

        temp = []
        strTemp= ""
        temp = entry[len(entry)-1].split('"')
        del temp[0]
        del temp[1]
        temp = temp[0].split(" ")
        del temp[0]
        for i in range(0,len(temp)):
            strTemp = strTemp + temp[i] + " "
        del entry[len(entry)-1]    
        entry.append(strTemp)

        #adds each entry into a complete list(2D array)
        clist.append(entry)

        #Removing the periods in the STQ sequence
        for c in clist:
            clist[clist.index(c)][6] = clist[clist.index(c)][6].replace(".","")

    #Parsing x to separate IDs by refseq
    for c in x:
        temp=x[x.index(c)][0].split("|")
        temp.append(x[x.index(c)][1])
        x[x.index(c)]=temp
    #Replacing/Inserting Amino acid sequence into Input
    fasta_entry=[]
    splitSeq = []
    i=0
    for c in clist:
        a= [t for t in x if t[3] == c[1]][0]
        fasta_entry.append(a)
        splitSeq.append(a[5][:int(c[4])-1])
        splitSeq.append(a[5][int(c[5]):])
        str = splitSeq[0]+clist[i][6]+splitSeq[1]
        x[i][5]=str
        x[i][5]=x[i][5].replace("\"","")
        i=i+1
    return x


















        
