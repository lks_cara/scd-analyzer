import os,pdb,datetime
from os.path import splitext,basename
import scd_psscan as scd
import csv
import itertools as it
import simplejson as json


# these are the settings the script uses
sett = {"outfolder": "ortholog_results","file_format": "fasta","reviewed":"yes","match": "","n": 3,"range":100}
log_file = "SQTQ_Orthologs.log"
log_level = 4
root_dir = os.path.dirname(os.path.realpath(__file__))

def find_orthologs(scd_prots,acc_from,acc_to):
    """ acc_from,acc_to: 9606
        scd_prot: list of scd proteins
    """
    count = 0
    ret_hits = {}
    for s in scd_prots:
        # find all the ortholog entries
        entries = [y for y in group_by_organism[acc_from] if s == y["protacc"]]
        # find all the corresponding orthologs for the target organism with the same HID
        found = False       
        for e in entries:   
            ret_hits[e["protacc"]] = []
            for g in group_by_organism[acc_to]:
                if g["hid"] == e["hid"]:
                    ret_hits[e["protacc"]].append(g["protacc"])
                    count += 1
                    break
    # remove all proteins that do not have orthologs 
    results = {}
    for k,v in ret_hits.iteritems():
        if len(v) != 0:
            results[k] = v   
    return results

def filter_out_non_SCD(scd_prot_with_ortho,possible_ortholog_scd):
    ret_hits = {}
    for prot,orthos in scd_prot_with_ortho.iteritems():
        ret_hits[prot] = []
        # checks each returned ortholog and sees if it exists in the list of scd proteins of the target organism
        for o in orthos:
            if len([y for y in possible_ortholog_scd if y == o]) != 0:
                ret_hits[prot].append(o)
    # leave out all proteins that do not have orthologs with SCDs
    results = {}
    for k,v in ret_hits.iteritems():
        if len(v) != 0:
            results[k] = v 
    return results

def log(msg,level=2):
	lev = {0: "ERROR",1:"WARNING",2:"INFO",3:"DEBUG"}
	now = datetime.datetime.now()
	line = "{}:\t{}\t{}\n".format(now,lev[level],msg)

	# output the log to the console and the file
	if level <= log_level:
		print line
		with open(log_file,"a") as f:
			f.write(line)


# IMPORTANT does not do ortholog analysis currently.
def scd_ortho_analyzer(scd_db_path,num_scd = [3],aa_range=[50,100]):
	# loads the ortholog files
	cols = ["hid","taxid","geneid","genesymb","protgi","protacc"]
	homologene_path = os.path.join(scd_db_path,"homologene.data")
	taxid_taxname_path = os.path.join(scd_db_path,"taxid_taxname.txt")
	if not os.path.exists(homologene_path): 
		print "ERROR: Could not find {} file. Needed for ortholog analysis.".format(homologene_path)
		raise LookupError
	if not os.path.exists(taxid_taxname_path):
		print "ERROR: Could not find {}. Needed to convert NCBI id to standard tax names.".format(taxid_taxname_path)
		raise LookupError
	# homolegen is causing a memory leak
	#homologene = open(homologene_path).read().split("\n")
	#homologene = [dict(zip(cols,[x.split(".")[0] for x in y.split("\t")])) for y in homologene if y != ""]

	# load the ncbi codes to tax name
	cols = ["id","org"]
	taxnames = open(os.path.join(scd_db_path,"taxid_taxname.txt")).read().split("\n")
	tax= [y.split("\t") for y in taxnames if y != ""]
	taxnames = {}
	for t in tax:
	    taxnames[t[0]] = t[1]
	      
	#group_by_organism = {}
	#for hom in homologene:
	#    if hom["taxid"] not in group_by_organism.keys(): group_by_organism[hom["taxid"]] = []
	#    group_by_organism[hom["taxid"]].append(dict(zip(["protacc","hid"],[hom["protacc"],hom["hid"]])))


	# runs the SCD analysis, which returns a list of files containing the results
	for ns in num_scd:
		for ar in aa_range:
			scd_files = scd.run_scd_dir(os.path.join(scd_db_path,"fasta"),os.path.join(scd_db_path,"scd_results"),SCD_definitions=[{"num_scd": ns ,"length_scd": ar}])


def scd_by_refseqids(scd_db_path,refseqids_path,organism,results_dir):
	''' Compares the IDs in refseqids_path file against each of the 
	.gff files generated for the organism. Returns a dictionary that is [SCD definition]: [number_of_proteins].
	For example: {"5SCD100": 102} would mean that 102 proteins have 5 S\TQ within 100 amino acids.
	'''
	data_dir = os.path.join(scd_db_path,"scd_results")
	query_refseqids = [x for x in open(refseqids_path).read().split("\n") if x != ""]
	taxid_taxname_path = os.path.join(scd_db_path,"taxid_taxname.txt")
	organism_to_taxid = [x.split("\t") for x in open(taxid_taxname_path).read().split("\n") if x != ""]
	organism_to_taxid = {value: key for (key, value) in organism_to_taxid}
	taxid = organism_to_taxid[organism]
	# find all .gff result files for the current organism
	data_files = [ os.path.join(data_dir,f) for f in os.listdir(data_dir) if os.path.isfile(os.path.join(data_dir,f)) 
													and f.endswith("_ps_scan_length_filt_mult_filt_isoform_filt.gff") 
													and f.split("_")[1]==taxid]
	results = {}

	prog_cur = 0
	progpath = os.path.join(results_dir,".prog")
	for f in data_files:
		_writeprogress("Searching for SCDs", prog_cur, len(data_files),progpath)
		scd_definition = basename(f).split("_")[0]
		# extract refseqids from the .gff file
		refseqids_confirmed = [x.split("|")[3].split(".")[0] for x in open(f).read().split("\n") if x != ""]
		results[scd_definition] = [x for x in query_refseqids if x in refseqids_confirmed]
	_writeprogress("Searching for SCDs", prog_cur, len(data_files),progpath)

	scd_defs = [] # the different definitions of SCD ex: '3SCD100'
	scd_data = []
	for k,v in results.items():
		scd_defs.append(k)
		scd_data.append(v)

	# output the results
	outpath = os.path.join(results_dir,"results.txt")
	with open(outpath,"wb") as writer:
		writer.write("\t".join(scd_defs)+"\n")
	with open(outpath, 'ab') as f:
		csv.writer(f,delimiter="\t").writerows(it.izip_longest(*scd_data))
	#return results

def _writeprogress(message,prog_cur,prog_max,path):
	json.dumps({"message":message,"prog_cur":prog_cur,"prog_max":prog_max})
		