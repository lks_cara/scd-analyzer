import os,pdb,datetime,subprocess
from os.path import splitext,basename
import utilities
import simplejson
import traceback
from itertools import *
from contextlib import nested
from zipfile import ZipFile


# these are the settings the script uses
sett = {"outfolder": "SCD_results",
		"file_format": "fasta", "reviewed": "yes", "match": "", "n": 3, "range": 100}
log_level = 4
root_dir = os.path.dirname(os.path.realpath(__file__))

summary_html = """<center>
<span style='color: black;'><b><u>Summary for **num** SCD in **range**</u></b></span><br/>
</center>

<center style="margin-top:1em; margin-bottom:1em" >
<a href='#' onclick="getProteinList('**restrictive_link**','Single SCD per protein')"<b>Proteins with SCDs: </b>**restrictive_count**</a>
</center>
<div  class="accordion" id="accordion**id**">
			<div class="accordion-group">
		<div class="accordion-heading">
			<center>
			<h5 class="accordion-toggle" data-toggle="collapse" style="text-decoration:underline;padding: 1px;margin: 1px;" data-parent="#accordion**id**" href="#accordionrefseq**id**">Advanced results</h5></center>
		</div>
		<div id="accordionrefseq**id**" class="collapse" style="padding-left:1em">
		<a href='#' onclick="getProteinList('**all_link**','All Proteins')"><b>Show multiple SCDs per protein: </b>**all_count**</a>		
		<br/><a href='#' onclick="getProteinList('**single_link**','Multiple isoforms')"><b>Show multiple isoforms: </b>**single_count**</a>
		</div>
	</div>
</div><br/>
	"""

def perform_ps_scan(fasta_file):
	'''Calls the Perl Script that searches through the fasta file for proteins with SCD domains.
	The SCD domains that our counted have extended distances.  For example, a search for 3 SCDs within
	50 AA would actualy return SCDs domains within 100 AA.  To filter to only include 50 AA, use the
	perform_length_restriction() function
	Returns the results as a list.
	'''
	# create query, n is the number of SQT to find in the 'range'
	query = (int(sett["n"])-1)*"[ST]Q-X(0,{})-".format(str(sett["range"]))+"[ST]Q"	
	program_name = "perl"
	arguments = [os.path.join(root_dir,"ps_scan","ps_scan.pl"), "-g","-ogff","-p"+query,fasta_file]
	if not os.path.exists(sett['outfolder']):
		os.makedirs(sett['outfolder'])
	outpath = os.path.join(sett['outfolder'],str(sett["n"])+"SCD"+str(sett["range"])+"_"+splitext(basename(fasta_file))[0] + "_ps_scan.gff")
	if os.path.exists(outpath): return outpath
	log("Running ps_scan Perl script with arguments: {}".format(arguments),2)
	command = [program_name]
	command.extend(arguments) #combines the 'perl' command with its arguments into one list
	out_writer = open(outpath,"wb")
	out = subprocess.Popen(command,stdout=out_writer,stderr=subprocess.PIPE)
	retcode = out.wait()
	out_writer.close()
	data = open(outpath).read()
	log("Number of proteins found with ps_scan\t"+ str(len(data.split('\n'))))
	
	if not os.path.exists(sett['outfolder']): os.mkdir(sett['outfolder'])
	# write out header line to summary html string.
	sum_html = summary_html
	sum_html = sum_html.replace("**num**",str(sett["n"])).replace("**range**",str(sett["range"])).replace("**id**",str(sett["n"])+"_"+str(sett["range"]))
	return [outpath,sum_html]


def perform_length_restriction(gff_file,sum_html):
	''' Filters out all SCD results that have a greater length than the desired range.
	'''
	outpath = os.path.join(sett['outfolder'],
		splitext(basename(gff_file))[0] + "_length_filt.gff")
	if os.path.exists(outpath): return outpath
	data,count = open(gff_file).read(),0
	with open(outpath,'wb') as out:
		for line in data.split("\n"):				
			if not line == "":
				start,end = line.split()[3],line.split()[4]					
				if (int(end) - int(start)) < sett['range']:
					out.write(line + "\n")
					count+=1
	sum_html = sum_html.replace("**all_link**",basename(outpath)).replace("**all_count**",str(count))			
	log("Number of proteins after length filtering\t"+str(count))
	return [outpath,sum_html]

def perform_multiple_removal(gff_file,sum_html):
	# remove duplicate protein hits
	outpath = os.path.join(sett['outfolder'],splitext(basename(gff_file))[0] + "_mult_filt.gff")
	if os.path.exists(outpath): return outpath
	added_name,name,count = [],"",0
	
	data = open(gff_file).read()
	with open(outpath,'wb') as out:
		for line in data.split('\n'):
			if not line == "": 
				linesplt = line.split("|")
				name = linesplt[3]
				desc = linesplt[-1].strip()
				# if the protein has not already returned, add it
				if name not in added_name: 
					out.write(line + "\n")
					added_name.append(name)
					count += 1

	sum_html = sum_html.replace("**single_link**",basename(outpath)).replace("**single_count**",str(count))	
	log("Number of proteins after duplicates removed\t"+str(count))
	
	return [outpath,sum_html]

def duplicate_description_removal(gff_file,sum_html):
		# Remove proteins that have the same protein description, but a different ID
	outpath = os.path.join(sett['outfolder'],splitext(basename(gff_file))[0] + "_mult_filt_restrictive.gff")
	if os.path.exists(outpath): return outpath
	added_name,added_description,name,count = [],[],"",0
	
	data = open(gff_file).read()
	d_writer = open(gff_file+".duplicates",'wb')
	with open(outpath,'wb') as out:
		for line in data.split('\n'):
			if not line == "": 
				linesplt = line.split("|")
				name = linesplt[3]
				desc = linesplt[-1].strip()
				# if the protein has not already returned, add it
				if name not in added_name and desc not in added_description: 
					out.write(line + "\n")
					added_name.append(name)
					added_description.append(desc)
					count += 1
				elif desc in added_description:
					d_writer.write(name+"\t"+desc+"\n")
	sum_html = sum_html.replace("**restrictive_link**",basename(outpath)).replace("**restrictive_count**",str(count))
	log("Number of proteins after restrictive search\t"+str(count))
	return [outpath,sum_html]


def perform_isoform_restriction(gff_file,sum_html):
	# removes isoforms by analyzing the description 
	outpath = os.path.join(sett['outfolder'],splitext(basename(gff_file))[0] + "_isoform_filt.gff")
	if os.path.exists(outpath): return outpath
	data,count,count_iso,count_iso_rem = open(gff_file).read(),0,0,0
	added = [] # keeps track of which protein isoforms have been added by comparing the descriptions string
	with open(outpath,'wb') as out:
		for line in data.split("\n"):
			if not line == "":
				name = line.split("|")[-1].split("isoform")
				# remove the isoform number character and rejoin the description separated by the isoform
				name[0] = name[0] + ' '.join(name[-1].strip().split(' ')[1:])

				# 'isoform' was found in description. add the description to the added list and print out the protein result
				if len(name) > 1: 
					count_iso +=1
					if name[0] not in added:
						added.append(name[0])
						out.write(line + "\n")
						count+=1
					else:
						count_iso_rem +=1
				else:
					out.write(line + "\n")
					count+=1
	log("Number of proteins with isoforms\t"+str(count_iso))
	log("Number of proteins with isoforms removed\t"+str(count_iso_rem))
	log("Number of proteins after isoform filtering\t"+str(count))
	sum_html = sum_html.replace("**isoform_link**",basename(outpath)).replace("**isoform_count**",str(count))
	[outpath,sum_html] = duplicate_description_removal(outpath,sum_html)
	return [outpath,sum_html]

def log(msg,level=2):	
	lev = {0: "ERROR",1:"WARNING",2:"INFO",3:"DEBUG"}
	now = datetime.datetime.now()
	line = "{}:\t{}\t{}\n".format(now,lev[level],msg)
	if not os.path.exists(sett["outfolder"]): return
	log_file = os.path.join(sett["outfolder"],".log")
	# output the log to the console and the file
	if level <= log_level:
		with open(log_file,"a") as f:
			f.write(line)

def run_scd_dir(fasta_dir,outfolder, file_format= "fasta",reviewed= "yes",match= "",SCD_definitions=[],perform_coloring = False):
	'''
	This function exists so that the code can be run from other python scripts.
	Returns the path containing all SCD proteins.
	Run analysis on all fasta files in the 'fasta_dir' directory.
	First, finds all matches.  Then filters matches to the restricted length ('All proteins' list or 'all').
	Then filters out hits from the same protein ('Mutilpe isoforms' or 'single' list).
	Then restricts results to show only a single isoform for proteins with multiple isoforms ('Single SCD per protein' or 'isoforms')
	'''
	global sett
	# runs the analysis for each SCD definition given
	progpath = os.path.join(outfolder,".prog")
	try:
		for i_s,s in enumerate(SCD_definitions):
			sett = {"outfolder": outfolder ,"file_format": file_format ,"reviewed": reviewed,"match": match ,"n": s["num_scd"] ,"range": s["length_scd"]}
			log_file = os.path.join(outfolder,"SQTQ.log")
			files_fasta = [os.path.join(fasta_dir,f) for f in os.listdir(fasta_dir) if ".fasta" in f]
			results = []
			for i,f in enumerate(files_fasta):
				_writeprogress("{} of {} definitions: Finding SCD domains".format(i_s+1,len(SCD_definitions)), 1, 4, progpath)
				[res_path,sum_html] = perform_ps_scan(f)
				[res_path,sum_html] = perform_length_restriction(res_path,sum_html)
				_writeprogress("{} of {} definitions: Preparing results for visualization. This may take a while.".format(i_s+1,len(SCD_definitions)), 2, 4, progpath)
				# Create the colored fasta sequence and include duplicates
				if perform_coloring:
					utilities.color_scd_json(f, res_path,"_all")
				_writeprogress("{} of {} definitions: Preparing results without duplicates".format(i_s+1,len(SCD_definitions)), 3, 4, progpath)			
				[res_path,sum_html] = perform_multiple_removal(res_path,sum_html)
				if perform_coloring:
					utilities.color_scd_json(f, res_path,"_single")
				_writeprogress("{} of {} definitions: Preparing results without isoforms".format(i_s+1,len(SCD_definitions)), 4, 4, progpath)			
				[res_path,sum_html] = perform_isoform_restriction(res_path,sum_html)
				if perform_coloring:
					utilities.color_scd_json(f, res_path,"_isoforms")
				results.append(res_path)
				with open(os.path.join(	sett["outfolder"],"summary.html"),"a") as writer:
					writer.write(sum_html)
		# group results for different definitions into single result files
		combine_zip_resultfiles(outfolder)
		_writeprogress("Analysis completed!", 100, 100, progpath)
	except Exception, e:
		log(traceback.format_exc(),0)
		_writeprogress("ERROR: Analysis failed.", 100,100,progpath)
	# causing a memory leak
	#return results

def run_scd_file(fasta_path,outfolder, file_format= "fasta",reviewed= "yes",match= "",SCD_definitions = [],perform_coloring = False):
	'''
	This function exists so that the code can be run from other python scripts.
	Returns the path containing all SCD proteins.
	Run analysis on a single single fasta_file passed in fasta_path
	'''
	global sett
	progpath = os.path.join(outfolder,".prog")
	# runs the analysis for each SCD definition given
	try:
		for i_s,s in enumerate(SCD_definitions):
			sett = {"outfolder": outfolder ,"file_format": file_format ,"reviewed": reviewed,"match": match ,"n": s["num_scd"] ,"range": s["length_scd"]}
			_writeprogress("{} of {} definitions: Finding SCD domains".format(i_s+1,len(SCD_definitions)), i_s, len(SCD_definitions), progpath)	
			[res_path,sum_html] = perform_ps_scan(fasta_path)
			[res_path,sum_html] = perform_length_restriction(res_path,sum_html)
			# Create the colored fasta sequence and include duplicates
			if perform_coloring:
				_writeprogress("{} of {} definitions: Preparing SCD results".format(i_s+1,len(SCD_definitions)), i_s, len(SCD_definitions), progpath)
				utilities.color_scd_json(f, res_path)			
			[res_path,sum_html] = perform_multiple_removal(res_path,sum_html)
			[res_path,sum_html] = perform_isoform_restriction(res_path,sum_html)
			with open(os.path.join(	sett["outfolder"],"summary.html"),"a") as writer:
				writer.write(sum_html)
		combine_zip_resultfiles(outfolder)
		_writeprogress("Analysis completed!", 100, 100, progpath)
	except Exception, e:
		log(traceback.format_exc(),0)
		_writeprogress("ERROR: Analysis failed.", 100,100,progpath)



def _writeprogress(message,prog_cur,prog_max,path):
	with open(path,'wb') as out:
		simplejson.dump({"status":message,"curprog":prog_cur,"progmax":prog_max},out)


def combine_zip_resultfiles(res_dir):
	res_types = {"_length_filt.gff":["all.csv","All SCD result for each protein"],
				"_ps_scan_length_filt_mult_filt.gff":["single.csv", "Only one SCD result per protein"],
				"_ps_scan_length_filt_mult_filt_isoform_filt.gff":["isforms.csv","Only single SCD result per isoform"]}
	run_files = [os.path.join(res_dir,f) for f in os.listdir(res_dir) 
				if os.path.isfile(os.path.join(res_dir,f)) and f.endswith(".gff")]

	result_files = []
	for k,v in res_types.iteritems():
		r_files = [f for f in run_files if f.endswith(k)]
		# combine all results, column wise, into a single file.
		outpath = os.path.join(res_dir,v[0])
		with open(outpath,'wb') as writer:
			# write out header and column names
			writer.write("#"+v[1]+"\n")
			writer.write(",".join([base_name(f).split("_")[0] for f in r_files])+"\n")			
			with nested(*(open(file) for file in r_files)) as handles:
				for tuple in izip_longest(*handles):
					writer.write(",".join("" if x == None else x.replace("\n","").replace(",",";") for x in tuple)+"\n")
		result_files.append(outpath)
	with ZipFile(os.path.join(res_dir,"SCD_results.zip"),'w') as zip:
		for f in result_files:
			zip.write(f,os.path.basename(f))

		for f,v in {"summary.html":"summary.html",".log":"log.txt"}.iteritems():
			f_path = os.path.join(res_dir,f)
			if os.path.exists(f_path): 
				zip.write(f_path,v)


def base_name(k):
	return os.path.basename(k).split(".")[0]

if __name__ == "__main__":
	files_fasta = [f for f in os.listdir(os.getcwd()) if os.path.isfile(f) and ".fasta" in f]
	for f in files_fasta:
		res_path = perform_ps_scan(f)
		res_path = perform_length_restriction(res_path)
		res_path = perform_multiple_removal(res_path)
		res_path = perform_isoform_restriction(res_path)

