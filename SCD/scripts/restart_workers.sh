#! /bin/bash
# Stop all currently running workers
ps auxww | grep 'SCD.worker_scd' | awk '{print $2}' | xargs kill -9
# Start up a worker with 2 threads
celery worker --app=SCD.worker_scd --loglevel=INFO -c 2 &
