import cherrypy, os, cgi, tempfile, sys, itertools
from mako.template import Template
from mako.lookup import TemplateLookup
from mako.exceptions import RichTraceback as MakoTraceback
from contextlib import closing
import re
from operator import attrgetter
from multiprocessing import Process
import threading
import cPickle
from operator import itemgetter
import logging
from logging import FileHandler,StreamHandler
import json
import pdb
from time import gmtime, strftime
import simplejson
import string
import random
import traceback
import argparse
import worker_scd
import sqlite3 as lite
import utilities as ut
from Bio import SeqIO
import subprocess
import celeryconfiguration
import sqlite3 as lite
import db_creator

baseuri = ""
root_dir = os.path.dirname(os.path.realpath(__file__))
lookup = TemplateLookup(directories=[os.path.join(root_dir,"frontend/templates")])
taxid_to_organism = {}

sett = {}

DEBUG_MODE = True

logger = logging.getLogger('scd-analyzer.server')
hdlr = logging.FileHandler(os.path.join(root_dir,'scd-analyzer_server.log'))
hdlr_std = StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.addHandler(hdlr_std)
logger.setLevel(logging.INFO)

root_dir = os.path.dirname(os.path.realpath(__file__))
static_dir = os.path.abspath(os.path.join(root_dir, "frontend/static"))
media = os.path.abspath(os.path.join(".","frontend/media"))
uploads_dir,results_dir = None,None

# Each function in this class is a web page 
class WebUI(object):
	def __init__(self):
		print "Preparing database"	
		tmp = worker_scd.ortho_analyzer.delay(sett["data_dir"],aa_range=sett["aa_range"],num_scd=sett["num_scd"])
		if sett["createdb"] or not os.path.exists(sett["db_path"]): db_creator.createDatabase(sett["db_path"])
		self._index_html = {}

	@cherrypy.expose
	def index(self):
		html_fasta_aa_range = "<select name='fasta_aa_range' class='selectpicker'>" + \
			"\n".join(["<option value='{}'>".format(str(x))+str(x)+"</option>" for x in sett["aa_range"]]) + "</select>"
		html_fasta_num_scd = "<select name='fasta_num_scd' class='selectpicker'>" + \
			"\n".join(["<option value='{}'>".format(str(x))+str(x)+"</option>" for x in sett["num_scd"]]) + "</select>"
		tmpl = lookup.get_template("index.html")

		return tmpl.render(fasta_aa_range = html_fasta_aa_range,fasta_num_scd=html_fasta_num_scd,organisms=_get_organism())

	@cherrypy.expose
	def query_by_fasta(self,txtFastaN=None,txtFastaAA=None,fasta_text=None,inputfastafiles=None):
		fastafile = inputfastafiles
		[id,up_dir,res_dir] = _generate_id()		
		fasta_path = os.path.join(up_dir,"user.fasta")			
		scd_definitions = _parse_scd_definitions("".join([txtFastaN,"SCD",txtFastaAA]))
		if len(scd_definitions) == 0:
			tmpl = lookup.get_template("error.html")
			return tmpl.render(message="ERROR: SCD definition is invalid. Please correct and then resubmit.")
		scd_definitions = [scd_definitions[0]]
		if not isinstance(inputfastafiles,list): inputfastafiles = [inputfastafiles] # convert to list if a single file is uploaded
		# Upload user FASTA files to the server
		if fastafile.filename != "":
			fasta_filename = fastafile.filename.split(".")[0] + ".fasta" # prevents files with multiple extensions
			f_path = os.path.join(up_dir,fasta_filename)
			if not os.path.exists(f_path):
				with open(f_path,"wb") as out:
					if fastafile != None and fastafile.filename != "":
						while True:
							data = fastafile.file.read(8192)
							if not data:
								break
							out.write(data)
		elif fasta_text != "":
			# Upload FASTA text input to server
			with open(fasta_path,"wb") as writer:				
				writer.write(fasta_text)
		else:
			tmpl = lookup.get_template("error.html")
			return tmpl.render(message="ERROR: Please upload FASTA data.")


		#p = threading.Thread(target=scd_psscan.run_scd_dir, args=(up_dir,res_dir,"fasta","yes","",scd_definitions,True))  
		#p.start()
		worker_scd.run_scd_dir.delay(up_dir,res_dir,"fasta","yes","",scd_definitions,True)
		raise cherrypy.HTTPRedirect(baseuri + "results_fasta?id=%s" % id)


	@cherrypy.expose
	def query_allproteins(self,organism, scd_defs=None, **kwargs):
		scd_defs = _parse_scd_definitions(scd_defs)
		# check if definition was uploaded
		if len(scd_defs) == 0:
			tmpl = lookup.get_template("error.html")
			return tmpl.render(message="ERROR: Please add a definition.")
		[id,up_dir,res_dir] = _generate_id()
		fasta_path = os.path.join(sett["data_dir"],"fasta",organism+".fasta")
		worker_scd.run_scd_file.delay(fasta_path,res_dir,"fasta","yes","",scd_defs,False)

		raise cherrypy.HTTPRedirect(baseuri + "results_allproteins?id=%s" % id)



	@cherrypy.expose
	def results_allproteins(self,id):
		# Loads the progress file if it exists
		output_dir = os.path.join(results_dir,id)
		p = {"status":"","curprog":0,"progmax":0}
		progress_path = os.path.join(output_dir,".prog")
		if os.path.exists(progress_path):
			with open(progress_path) as f:
				p = json.loads(f.read() )

		tmpl = lookup.get_template("results_allproteins.html")
		return tmpl.render(run_id=id,curprog = p["curprog"],progmax = p["progmax"],status=p["status"],
							zipfile="results/"+id+"/SCD_results.zip")

	@cherrypy.expose
	def results_fasta(self,id):
		input_dir = os.path.join(uploads_dir,id)
		output_dir = os.path.join(results_dir,id)
		# Loads the progress file if it exists
		p = {"status":"","curprog":0,"progmax":0}
		progress_path = os.path.join(output_dir,".prog")
		if os.path.exists(progress_path):
			with open(progress_path) as f:
				p = json.loads(f.read() )

		# gather the input file names, used to create the result tabs
		fasta_file_names = [base_name(f) for f in os.listdir(input_dir) if os.path.isfile(os.path.join(input_dir,f)) and f.endswith(".fasta")]
		tmpl = lookup.get_template("results_fasta.html")
		return tmpl.render(run_id=id,fasta_name=fasta_file_names[0],curprog = p["curprog"],progmax = p["progmax"],status=p["status"])

	@cherrypy.expose
	def get_json_results(self,id,result_name):
		json_path = os.path.join(results_dir,id,result_name+".json")
		if os.path.exists(json_path):
			return open(json_path).read()
		return ""
		
	@cherrypy.expose
	def get_log(self,id):
		txt_path = os.path.join(results_dir,id,".log")
		if os.path.exists(txt_path):
			return open(txt_path).read()
		return ""

	@cherrypy.expose
	def get_progress(self,id):
		json_path = os.path.join(results_dir,id,".prog")
		if os.path.exists(json_path):
			return open(json_path).read()
		return ""

	@cherrypy.expose
	def get_summary(self,id):
		sum_path = os.path.join(results_dir,id,"summary.html")
		if os.path.exists(sum_path):
			return open(sum_path).read()
		return ""

	@cherrypy.expose
	def get_allprots_results(self,id,res_filename):
		res_path = os.path.join(results_dir,id,res_filename)
		if os.path.exists(res_path):
			return open(res_path).read()
		return ""

	@cherrypy.expose
	def get_sequence(self,ref_id):
		print os.path.join(sett["data_dir"],"SCD.db")
		con = lite.connect(os.path.join(sett["data_dir"],"SCD.db"))
		with con:
			cur = con.cursor()
			cur.execute("SELECT RefSeq, Description,Sequence FROM fasta WHERE RefSeq == '{}'".format(ref_id))
			data = cur.fetchone()
		return json.dumps(data);

	@cherrypy.expose
	def about(self):
		tmpl = lookup.get_template("about.html")
		return tmpl.render()

	@cherrypy.expose
	def contact(self):
		tmpl = lookup.get_template("contact.html")
		return tmpl.render()


	
def base_name(k):
	return os.path.basename(k).split(".")[0]

def _generate_id():
	'''generate random id and reserve a place in the uploads and results folder
	'''
	id = ''.join(random.choice(string.lowercase+string.digits) for _ in range(32))
	while (os.path.exists(os.path.join(uploads_dir,id))):
					id = ''.join(random.choice(string.lowercase+string.digits) for _ in range(32))
	res_dir = os.path.join(results_dir,str(id))
	up_dir = os.path.join(uploads_dir,str(id))
	if not os.path.exists(res_dir):
			os.mkdir(res_dir)
	if not os.path.exists(up_dir):
			os.mkdir(up_dir)
	return [id,up_dir,res_dir]

def _get_organism():
	data_dir = os.path.join(sett["data_dir"],"scd_results")
	data_files = [ f for f in os.listdir(data_dir) if os.path.isfile(os.path.join(data_dir,f)) and f.endswith(".gff") ]
	organisms = set(x.split("_")[1] for x in data_files)
	str_organisms = [taxid_to_organism[x] for x in organisms]
	html = ""
	for i,o in enumerate(organisms):
		# set the default selected value to be homo sapiens
		if o == "9606":	html += "<option value={} selected='selected'>{}</option>".format(o,str_organisms[i])
		else:	html += "<option value={}>{}</option>".format(o,str_organisms[i])
	return html

def _parse_scd_definitions(scd_defs):
	''' Parses scd_definitions string.  Should be in the format of:
		3SCD100;4SCD299
	'''
	scd_definitions = []
	# remove whitespace and split into list of definitions
	scd_defs = "".join(scd_defs.split()).split(";")
	for scd_df in scd_defs:
		s = scd_df.split("SCD")
		if len(s) != 2:
			continue
		# convert values to integer
		try:
			s[0]  = int(s[0])
			s[1] = int(s[1])
		except:
			continue
		scd_definitions.append(dict(zip(["num_scd","length_scd"],s)))
	return scd_definitions




if __name__ == "__main__":
	global static_dir,results_dir,media,root_dir,sett,uploads_dir,taxid_to_organism,baseuri
	parser = argparse.ArgumentParser(prog="python -m grsnp.server", description="Starts the SCD Analyzer server. Example: python -m grsnp.server -d /home/username/scd-analyzer/ -p 8000", epilog="Use SCD Analyzer: http://localhost:8000/scd")
	parser.add_argument("--data_dir" , "-d", nargs="?", help="Set the directory containing the database. Required. Use absolute path. Example: /home/username/scd_db/.", default="")
	parser.add_argument("--port","-p", nargs="?", help="Socket port to start server on. Default: 8081", default=8081)
	parser.add_argument("--createdb", action='store_true', help="Recreate the DB needed by the server.")
	parser.add_argument("--num_workers", "-w", type=int, help="The number of celery workers to start. Default: 1", default=1)
	parser.add_argument("--production", "-r", action="store_true")
	args = vars(parser.parse_args())
	port = args["port"]
	if args["production"]:
		baseuri = "scd/"
	data_dir = args["data_dir"]
	if data_dir == "":
		data_dir = os.path.abspath(os.getcwd())

	# global settings used by GR
	sett = {"data_dir":os.path.join(data_dir,"scd_db"),
				"run_files_dir": os.path.join(data_dir,"scd_db","run_files_dir"),
				"custom_dir": os.path.join(data_dir,"custom_data"),
				"aa_range": [50,100],
				"num_scd": [3,5],
				"createdb": args["createdb"],
				"db_path": os.path.join(data_dir,"scd_db",'SCD.db')
				}

	#validate data directory
	if not os.path.exists(sett["data_dir"]):
		print "ERROR: {} does not exist.".format(os.path.join(sett["data_dir"]))
		sys.exit()	

	#load taxids_to_organisms
	taxid_to_organism = [x.split("\t") for x in open(os.path.join(sett['data_dir'],"taxid_taxname.txt")).read().split("\n") if x != ""]
	taxid_to_organism = {key: value for (key, value) in taxid_to_organism}

	# validate run_files directory
	if not os.path.exists(sett["run_files_dir"]): os.mkdir(sett["run_files_dir"])
	results_dir = os.path.join(sett["run_files_dir"],"results")
	uploads_dir = os.path.join(sett["run_files_dir"],"uploads")	
	if not os.path.exists(results_dir):
		os.mkdir(results_dir)
	if not os.path.exists(uploads_dir):
		os.mkdir(uploads_dir)
	if not os.path.exists(os.path.join(sett["data_dir"],"scd_results")): 
		os.mkdir(os.path.join(sett["data_dir"],"scd_results"))
	if port:
		cherrypy.server.max_request_body_size = 0
		cherrypy.config.update({
			"server.socket_port": int(port),
			"server.socket_host":"127.0.0.1"})
		conf = {"/static": 	
					{"tools.staticdir.on": True,
					"tools.staticdir.dir": static_dir},
				"/results": 
					{"tools.staticdir.on": True,
					"tools.staticdir.dir": os.path.abspath(results_dir)},
				"/data":
					{"tools.staticdir.on": True,
					"tools.staticdir.dir": os.path.abspath(sett["data_dir"])}
				}
		script = ["redis-server", "--port", str(celeryconfiguration.BROKER_PORT)]
		fh = open("NULL","w")
		out = subprocess.Popen(script,stdout=fh,stderr=fh)
		for i in range(args["num_workers"]):
			#script = ["celery","worker", "--app", "SCD.worker_scd", "--loglevel", "INFO", "-n", "scd{}.%h".format(i)]
			print "WORKER CREATED", script
			#tmp = subprocess.Popen(script,stdout=fh,stderr=fh)	
		
		cherrypy.quickstart(WebUI(), "/scd", config=conf)

	else:
		print "WARNING: No port given. Server not started. Use --port flag to set port."