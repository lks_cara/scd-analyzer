import pdb
import json
import os
from Bio import SeqIO
from itertools import *
from contextlib import nested

def parse_fasta(fasta_path):
	""" Parse the fasta file and return as list of dict("id", "sequence")
	"""	
	print "NEW"
	fs = open(fasta_path,'rU')
	data = SeqIO.parse(fs, "fasta")
	list_fasta=[]
	for i,d in enumerate(data):
		list_fasta.append({"id": d.description,"sequence": d.seq.tostring()})

	return list_fasta

def parse_results(result_path):
	""" Parse the results file and return as list of dict("id", "sequence")
	"""
	cols = ["id","scd_start","scd_end","scd_sequence","description"]
	result = open(result_path).read().strip()

	result_entries = [x for x in result.split("\n") if x != ""]
	if len(result_entries) == 0: return []
	result_entries = [x.split("\t") for x in result_entries]
	list_results = []
	for r in result_entries:
		seq = r[8].split("\"")[1].replace(".","")
		descript = r[-1].split("SequenceDescription")[-1].split("|")[-1].replace("\"","")
		list_results.append(dict(zip(cols,[r[0],r[3],r[4],seq,descript])))
	return list_results

def color_scd_json(fasta_path,result_path,postfix):
	"""  Prepare the results as in JSON. This is used to visualize the results in the browser.
	Result coloring is done on the client side for performance reasons.
	"""
	list_fasta = parse_fasta(fasta_path)
	list_result =  parse_results(result_path)
	for r in list_result:
		cur_fasta = [x for x in list_fasta if r["id"] in x["id"]][0]
		r["sequence"] = cur_fasta["sequence"].replace("\n","")
	json_path = os.path.join(os.path.dirname(result_path),base_name(fasta_path)+postfix+".json")
	json.dump(list_result,open(json_path,"wb"))	
	return json_path


def base_name(k):
    return os.path.basename(k).split(".")[0]


