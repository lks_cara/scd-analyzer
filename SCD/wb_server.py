import subprocess
import celeryconfiguration
import os
import cherrypy
import server


root_dir = os.path.dirname(os.path.realpath(__file__))
static_dir = os.path.abspath(os.path.join(root_dir, "frontend/static"))
media = os.path.abspath(os.path.join(".","frontend/media"))
num_workers = 2

data_dir = os.path.abspath(os.getcwd())
# global settings used by GR
sett = {"data_dir":os.path.join(data_dir,"scd_db"),
			"run_files_dir": os.path.join(data_dir,"scd_db","run_files_dir"),
			"custom_dir": os.path.join(data_dir,"custom_data"),
			"aa_range": [50,100],
			"num_scd": [3,5],
			"createdb": False,
			"db_path": os.path.join(data_dir,"scd_db",'SCD.db')
			}

server.sett = sett


#validate data directory
if not os.path.exists(sett["data_dir"]):
	print "ERROR: {} does not exist.".format(os.path.join(sett["data_dir"]))
	sys.exit()	

#load taxids_to_organisms
server.taxid_to_organism = [x.split("\t") for x in open(os.path.join(sett['data_dir'],"taxid_taxname.txt")).read().split("\n") if x != ""]
server.taxid_to_organism = {key: value for (key, value) in server.taxid_to_organism}

# validate run_files directory
if not os.path.exists(sett["run_files_dir"]): os.mkdir(sett["run_files_dir"])
results_dir = os.path.join(sett["run_files_dir"],"results")
uploads_dir = os.path.join(sett["run_files_dir"],"uploads")	
if not os.path.exists(results_dir):
	os.mkdir(results_dir)
if not os.path.exists(uploads_dir):
	os.mkdir(uploads_dir)
if not os.path.exists(os.path.join(sett["data_dir"],"scd_results")): 
	os.mkdir(os.path.join(sett["data_dir"],"scd_results"))

server.results_dir = results_dir
server.uploads_dir = uploads_dir

cherrypy.server.max_request_body_size = 0
cherrypy.config.update({
	"server.socket_port": 23231,
	"server.socket_host":"127.0.0.1"})
conf = {"/static": 	
			{"tools.staticdir.on": True,
			"tools.staticdir.dir": static_dir},
		"/results": 
			{"tools.staticdir.on": True,
			"tools.staticdir.dir": os.path.abspath(results_dir)},
		"/data":
			{"tools.staticdir.on": True,
			"tools.staticdir.dir": os.path.abspath(sett["data_dir"])}
		}
script = ["redis-server", "--port", str(celeryconfiguration.BROKER_PORT)]
fh = open("NUL","w")
out = subprocess.Popen(script,stdout=fh,stderr=fh)

for i in range(num_workers):
	script = ["celery","worker", "--app", "SCD.scd_psscan", "--loglevel", "INFO", "-n", "scd{}.%h".format(i)]
	out = subprocess.Popen(script,stdout=fh,stderr=fh)

cherrypy.quickstart(server.WebUI(), "/scd", config=conf)