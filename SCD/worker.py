from celery import Celery

app = Celery('SCD_analyzer')
app.config_from_object('SCD.celeryconfiguration')

if __name__ == "__main__":
	app.run()
