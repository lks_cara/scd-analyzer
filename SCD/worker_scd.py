from celery import Celery
import scd_psscan
import scd_analyzer

# celery
app = Celery('SCD')
app.config_from_object('SCD.celeryconfiguration')

@app.task(ignore_result=True)
def run_scd_dir(fasta_dir,outfolder, file_format= "fasta",reviewed= "yes",match= "",SCD_definitions=[],perform_coloring = False):
	scd_psscan.run_scd_dir(fasta_dir,outfolder, file_format,reviewed,match,SCD_definitions,perform_coloring)

@app.task(ignore_result=True)
def run_scd_file(fasta_path,outfolder, file_format= "fasta",reviewed= "yes",match= "",SCD_definitions = [],perform_coloring = False):
	scd_psscan.run_scd_file(fasta_path,outfolder, file_format,reviewed,match,SCD_definitions,perform_coloring)


@app.task(ignore_result=False)
def ortho_analyzer(scd_db_path,num_scd = [3],aa_range=[50,100]):
	scd_analyzer.scd_ortho_analyzer(scd_db_path,num_scd,aa_range)
