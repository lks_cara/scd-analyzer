from distutils.core import setup
import setuptools
import os


# Create list of data files with paths relative to the base genome-runner directory
files = ["frontend/*"]

setup(
    name='SCD Analyzer',
    version='0.1.0',
    author='Lukas R. Cara, Medina DC, Maia Larios-Sanz, Albert Ribes',
    author_email='lks_cara@yahoo.com',
    #package_data={'SCD': ['ps_scan/*'],'SCD': ['frontend/*']},
    #data_files=[('SCD', ['frontend']),],
    packages=['SCD'],
    package_dir={"SCD": "SCD"},
    package_data={"SCD": files},
    url='http://www.stthom.edu',
    license='LICENSE.txt',
    description='SCD Analyzer',
    long_description=open('README').read())